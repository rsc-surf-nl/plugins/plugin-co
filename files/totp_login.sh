#!/bin/bash
set -e

# This is to allow compatibility with pam_script.so and pam_exec.so
PAM_AUTHTOK=${PAM_AUTHTOK:-$(cat -)}

CO_USER_API_ENDPOINT=$(jq -r .co_user_api_endpoint < /etc/rsc/workspace.json)
CO_ROLES_ENABLES=$(jq -r .co_roles_enabled < /etc/rsc/workspace.json)
CO_PASSWORDLESS_SUDO=$(jq -r .co_passwordless_sudo < /etc/rsc/workspace.json)
CO_ADMIN_USER_ONLY=$(jq -r .co_admin_user_only < /etc/rsc/workspace.json)
CO_TOKEN=$(jq -r .co_token < /etc/rsc/workspace.json)
CO_USER_INFO=$(curl --silent -H "Authorization: ${CO_TOKEN}" "${CO_USER_API_ENDPOINT}" | jq -r '.')
FOUND_USER=0

while read -r user
do
    username=$(echo "${user}" | jq -r '.username')
    is_admin=$(echo "${user}" | jq 'select(.roles | index("src_co_admin"))')
    if [ "${username}" == "${PAM_USER}" ]; then
      FOUND_USER=1
      if [ "$CO_ADMIN_USER_ONLY" == "true" ] && [ -z "${is_admin}" ]; then
        echo "Skipping ${PAM_USER}. It's no co-admin user."
        exit 1
      fi
      continue
    fi
done < <(echo "${CO_USER_INFO}" | jq -c '.[]')

if [ "$FOUND_USER" -eq 0 ]; then
  echo "Skipping ${PAM_USER}. It's not found in the CO user list."
  exit 1
fi

TOTP_VERIFICATION_ENDPOINT="${CO_USER_API_ENDPOINT}${PAM_USER}/verify_totp/"
IS_TOTP_VALID=$(curl  -H 'Content-Type:Application/json' -H "Authorization: ${CO_TOKEN}" -d "{\"code\": \"${PAM_AUTHTOK}\"}"  "${TOTP_VERIFICATION_ENDPOINT}" | jq -e '.is_valid // false')

if [ "$IS_TOTP_VALID" == "true" ]; then
  echo "User ${PAM_USER} successfully logged in with TOTP token"
  grep -q "^${PAM_USER}:" /etc/passwd && exit 0
  adduser "${PAM_USER}" --disabled-password --quiet --gecos ""
  exit 0
fi

echo "Error [HTTP status: $HTTP_STATUS]"
exit 1
