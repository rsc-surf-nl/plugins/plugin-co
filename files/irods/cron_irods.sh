#!/bin/bash
set -e

CO_USER_API_ENDPOINT=$(jq -r .co_user_api_endpoint < /etc/rsc/workspace.json)
CO_TOKEN=$(jq -r .co_token < /etc/rsc/workspace.json)
CO_USER_INFO=$(curl --silent -H "Authorization: ${CO_TOKEN}" "${CO_USER_API_ENDPOINT}" | jq -r '.')

CO_USER_INFO_FILE='/etc/rsc/cron_irods.config'
echo "${CO_USER_INFO}" > "${CO_USER_INFO_FILE}"

for USER in $(cut -d: -f1,3 /etc/passwd | egrep ':[0-9]{4}$' | cut -d: -f1)
do
    /etc/rsc/setup_irods.sh "$USER" "$CO_USER_INFO_FILE" || true
done

rm -f "${CO_USER_INFO_FILE}"