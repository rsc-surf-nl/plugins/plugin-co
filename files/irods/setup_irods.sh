#!/bin/bash

set -e

IINIT=$(/usr/bin/which iinit)

# User dependent parameters
RC_USERNAME="$1"
CO_USER_INFO_FILE="$2"

CO_USER_INFO=$(cat "${CO_USER_INFO_FILE}")

do_log() {
    echo "`date "+%Y.%m.%d-%H:%M:%S %Z"` $@"
}

# get credentials from workspace.json
get_irods_config() {
    IRODS_CONFIG=$( echo $CO_USER_INFO | jq --arg USERNAME "${RC_USERNAME}" -r '.[] | select(.username==$USERNAME) | select(.services[].service_meta.category=="irods") | .services[0]')
    if [ "$IRODS_CONFIG" = "null" ]
    then
        exit 1
    fi
    echo ${IRODS_CONFIG}
}


# generates irods environment file and initializes password
configure_icommands () {
    if [ ! -e  $IRODS_ENVIRONMENT_FILE ] || [ ! -e $IRODS_AUTHENTICATION_FILE ]
    then
        do_log mkdir -p /home/${RC_USERNAME}/.irods/
        mkdir -p /home/${RC_USERNAME}/.irods/
        do_log "create ${IRODS_ENVIRONMENT_FILE}"
        IRODS_AUTHENTICATION_SCHEME=$(      echo ${IRODS_CONFIG} | jq -r '.irods | .irods_authentication_scheme // "PAM"' )
        IRODS_ENCRYPTION_NUM_HASH_ROUNDS=$( echo ${IRODS_CONFIG} | jq -r '.irods | .irods_encryption_num_hash_rounds // "16"' )
        IRODS_CLIENT_SERVER_POLICY=$(       echo ${IRODS_CONFIG} | jq -r '.irods | .irods_client_server_policy // "CS_NEG_REQUIRE"' )
        IRODS_ENCRYPTION_ALGORITHM=$(       echo ${IRODS_CONFIG} | jq -r '.irods | .irods_encryption_algorithm // "AES-256-CBC"' )
        IRODS_ENCRYPTION_SALT_SIZE=$(       echo ${IRODS_CONFIG} | jq -r '.irods | .irods_encryption_salt_size // "8"' )
        IRODS_SSL_VERIFY_SERVER=$(          echo ${IRODS_CONFIG} | jq -r '.irods | .irods_ssl_verify_server // "hostname"' )
        IRODS_ENCRYPTION_KEY_SIZE=$(        echo ${IRODS_CONFIG} | jq -r '.irods | .irods_encryption_key_size // "32"' )
        IRODS_PORT=$(                       echo ${IRODS_CONFIG} | jq -r '.irods | .irods_port // "1247"' )
        IRODS_CLIENT_SERVER_NEGOTITATION=$( echo ${IRODS_CONFIG} | jq -r '.irods | .irods_client_server_negotiation // "request_server_negotiation"' )
        cat <<EOF > ${IRODS_ENVIRONMENT_FILE}
    {
      "irods_host": "$IRODS_URL",
      "irods_port": $IRODS_PORT,
      "irods_user_name": "$IRODS_USERNAME",
      "irods_zone_name": "$IRODS_ZONE",
      "irods_authentication_scheme": "$IRODS_AUTHENTICATION_SCHEME",
      "irods_encryption_num_hash_rounds": $IRODS_ENCRYPTION_NUM_HASH_ROUNDS,
      "irods_client_server_policy": "$IRODS_CLIENT_SERVER_POLICY",
      "irods_encryption_algorithm": "$IRODS_ENCRYPTION_ALGORITHM",
      "irods_encryption_salt_size": $IRODS_ENCRYPTION_SALT_SIZE,
      "irods_ssl_verify_server": "$IRODS_SSL_VERIFY_SERVER",
      "irods_encryption_key_size": $IRODS_ENCRYPTION_KEY_SIZE,
      "irods_client_server_negotiation": "$IRODS_CLIENT_SERVER_NEGOTITATION"
    }
EOF
        chown -R "${RC_USERNAME}" "/home/${RC_USERNAME}/.irods"
        IRODS_ENVIRONMENT_FILE=$IRODS_ENVIRONMENT_FILE IRODS_AUTHENTICATION_FILE=$IRODS_AUTHENTICATION_FILE sudo -u ${RC_USERNAME} ${IINIT} <<- ANSWERS 
${IRODS_TOKEN} 
ANSWERS
    fi
}

irods_mount () {
    RC_USERNAME="$1"
    IRODS_USERNAME="$2"
    IRODS_PASSWORD="$3"
    IRODS_WEBDAV_URL="$4"
    if [ -n "${IRODS_USERNAME}" ] && [ -n "${IRODS_PASSWORD}" ]; then

	do_log mkdir -p "/home/${RC_USERNAME}/.davfs2/" "/home/${RC_USERNAME}/irods/"
        mkdir -p "/home/${RC_USERNAME}/.davfs2/" "/home/${RC_USERNAME}/irods/"

	do_log configure /home/${RC_USERNAME}/.davfs2/secrets
        echo "${IRODS_WEBDAV_URL} ${IRODS_USERNAME} ${IRODS_PASSWORD}" > "/home/${RC_USERNAME}/.davfs2/secrets"
        chown -R "${RC_USERNAME}" "/home/${RC_USERNAME}/.davfs2"
        chown "${RC_USERNAME}" "/home/${RC_USERNAME}/irods"
        chown 600 "/home/${RC_USERNAME}/.davfs2/secrets"

	do_log configure /etc/fstab "https://${IRODS_WEBDAV_URL} /home/${RC_USERNAME}/irods davfs rw,user,uid=${RC_USERNAME},username=${IRODS_USERNAME},noauto,_netdev 0 0"
        grep -q "/home/${RC_USERNAME}/irods" /etc/fstab || \
	    echo "https://${IRODS_WEBDAV_URL} /home/${RC_USERNAME}/irods davfs rw,user,uid=${RC_USERNAME},username=${IRODS_USERNAME},noauto,_netdev 0 0" >> /etc/fstab
	echo mount /home/${RC_USERNAME}/irods
        mount /home/${RC_USERNAME}/irods <<- ANSWERS
	${IRODS_PASSWORD}
	ANSWERS
    fi
}

# Script start
case "${1}" in
    root|ubuntu|"" )
        exit 0
    ;;
esac

IRODS_CONFIG=$( get_irods_config )

if [ -z "$IRODS_CONFIG" ]; then
    exit 0
fi

IRODS_USERNAME=$(   echo ${IRODS_CONFIG} | jq -r ' .irods | .username' )
IRODS_URL=$(        echo ${IRODS_CONFIG} | jq -r ' .irods | .url ' )
IRODS_TOKEN=$(      echo ${IRODS_CONFIG} | jq -r ' .id ' )
IRODS_ZONE=$(       echo ${IRODS_CONFIG} | jq -r ' .irods | .zone ' )
IRODS_WEBDAV_URL=$( echo ${IRODS_CONFIG} | jq -r ' .irods | .davrods_url ' )
IRODS_ENVIRONMENT_FILE=/home/${RC_USERNAME}/.irods/irods_environment.json
IRODS_AUTHENTICATION_FILE=/home/${RC_USERNAME}/.irods/.irodsA
IRODS_WEBDAV_PORT=$(  echo ${IRODS_CONFIG} | jq -r ' .irods | .davrods_port // "443"' )

# configure icommands
configure_icommands

# configure davfs
irods_mount ${RC_USERNAME} ${IRODS_USERNAME} ${IRODS_TOKEN} ${IRODS_WEBDAV_URL}
