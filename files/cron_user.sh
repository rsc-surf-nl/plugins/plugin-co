#!/bin/bash
set -e

CO_USER_API_ENDPOINT=$(jq -r .co_user_api_endpoint < /etc/rsc/workspace.json)
CO_ROLES_ENABLES=$(jq -r .co_roles_enabled < /etc/rsc/workspace.json)
CO_PASSWORDLESS_SUDO=$(jq -r .co_passwordless_sudo < /etc/rsc/workspace.json)
CO_ADMIN_USER_ONLY=$(jq -r .co_admin_user_only < /etc/rsc/workspace.json)
CO_TOKEN=$(jq -r .co_token < /etc/rsc/workspace.json)
CO_USER_INFO=$(curl --silent -H "Authorization: ${CO_TOKEN}" "${CO_USER_API_ENDPOINT}" | jq -r '.')
MANAGED_USERS_DIR="/etc/rsc/managedusers"
MANAGED_GROUPS_FILE="/etc/rsc/managedgroups"


# create managed group overview per user
if [ "$CO_ROLES_ENABLES" == "true" ]; then
  SRAM_ROLES=$(echo "$CO_USER_INFO" | jq -r '[ .[] | .roles[] ] | unique | flatten[]')
  IGNORE_ROLES="@all src_co_wallet src_no_ext_apps src_ws_admin src_developers"

  if [[ ! -e "${MANAGED_GROUPS_FILE}" ]]; then
    touch "${MANAGED_GROUPS_FILE}"
  fi
  chmod 700 "${MANAGED_GROUPS_FILE}"

  MANAGED_ROLES=$(cat "${MANAGED_GROUPS_FILE}") # Get managed roles stored on disk in previous run.
  echo "${SRAM_ROLES}" > "${MANAGED_GROUPS_FILE}" # Update managed roles on disk with latest info from SRAM.
fi


# Disable missing users
mkdir -p "${MANAGED_USERS_DIR}"
for file in "$MANAGED_USERS_DIR"/*
do
  [[ -f "${file}" ]] || break
  user=$(basename -- "${file}")
  if ! echo "${CO_USER_INFO}" | grep "\"${user}\"," &>/dev/null
  then
    /usr/sbin/usermod -s /usr/sbin/nologin "${user}" || true
    rm "${file}"
    echo "removing user ${user} from login"
  fi
done

echo "${CO_USER_INFO}" | jq -c '.[]' | while read -r user
do
    username=$(echo "${user}" | jq -r '.username')
    uid=$(echo "${user}" | jq -r '.integer_id')
    is_admin=$(echo "${user}" | jq 'select(.roles | index("src_co_admin"))')

    if [ "$CO_ADMIN_USER_ONLY" == "true" ] && [ -z "${is_admin}" ]; then
        echo Skipping "${username}". Is no co-admin user.
        touch "$MANAGED_USERS_DIR"/"$username"
        if id -u "${username}" &>/dev/null
        then
          /usr/sbin/usermod -s /usr/sbin/nologin "${username}" || true
        else
          /usr/sbin/useradd -u "${uid}" -s /usr/sbin/nologin "${username}" -m
        fi
        [[ -f "$MANAGED_USERS_DIR"/"$username"  ]] && echo "removing user ${username} from login because they are not admin anymore" && /usr/sbin/usermod -s /usr/sbin/nologin "${username}" || true
        continue
    fi
    touch "$MANAGED_USERS_DIR"/"$username"

    # To fix Centos problem in killing user processes in logout
    mkdir -p /var/lib/systemd/linger
    touch "/var/lib/systemd/linger/${username}"

    echo "creating user ${username} if not exist (Starting)"
    # Create user or re-enable it if it was removed in the past
    if id -u "${username}" &>/dev/null
    then
      /usr/sbin/usermod -s /bin/bash "${username}"
    else
      /usr/sbin/useradd -u "${uid}" -s /bin/bash "${username}" -m
    fi
    echo "creating user ${username} if not exist (Done)"

    if [ "$CO_ROLES_ENABLES" == "true" ]; then
        echo "Provisioning groups for user ${username} from SRAM roles"

        # Get the SRAM groups for this user from our json. These are groups to which the user should belong.
        user_roles=$(echo "${user}" | jq -r '.roles[]' | tr -d ' :' | grep -v '^$' | cut -c1-32)
        # Get all the roles in MANAGED_ROLES that aren't in user_roles. These are groups to which the user should not (no longer) belong.
        delete_roles=$(printf "%s\n%s" "${user_roles}" "${MANAGED_ROLES}" | sort | uniq -u)

        for group in $delete_roles; do
          if ! ( echo "$IGNORE_ROLES" | grep -w -q "$group" ); then
            echo "Deleting ${username} from ${group}"
            gpasswd -d "${username}" "${group}" || true
          fi
        done

        for group in $user_roles; do
          if ! ( echo "$IGNORE_ROLES" | grep -w -q "$group" ); then
            echo "Adding ${username} to ${group}"
            getent group "${group}" || groupadd  "${group}"
            gpasswd -a "${username}" "${group}" || true
          fi
        done

        if [ -n "${is_admin}" ]; then
           gpasswd -a "${username}" wheel || true
        else
          gpasswd -d "${username}" wheel || true
        fi
    fi





#if true, then add user to sudoers and make sudoers have root access without password while removing the user from the wheel group to prevent sudo conflict.
    if [ "$CO_PASSWORDLESS_SUDO" == "true" ]; then
        gpasswd -d "${username}" wheel || true
        gpasswd -a "${username}" sudoers || true
    else
        gpasswd -d "${username}" sudoers || true
    fi

    echo "Add all users to fuse group"
    gpasswd -a "${username}" fuse || true

    echo "Adding SSH keys to ${username}"
    echo "${CO_USER_INFO}" | jq --arg USERNAME "${username}" -r '.[] | select(.username == $USERNAME) | .ssh_keys[]' > /home/"${username}"/.ssh/authorized_keys
done
