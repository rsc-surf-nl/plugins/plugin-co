#!/usr/bin/env sh

MESSAGE_SENT_FLAG_FILE="/var/run/src_fs_usage_warning_sent"
export USAGE_PERCENTAGE=$(df --output=pcent / | tail --lines=1 | sed -r 's/\s|%//g')

if [ "${USAGE_PERCENTAGE}" -lt 80 ]; then
  rm --force "${MESSAGE_SENT_FLAG_FILE}"
  exit
fi

if [ -e "${MESSAGE_SENT_FLAG_FILE}" ]; then
  AGE=$(( $(date +"%s") - $(stat --format='%Y' "${MESSAGE_SENT_FLAG_FILE}") ))
  if [ "${AGE}" -lt 259200 ]; then
    # Do not re-send warnings within three days
    exit
  fi
fi

CO_TOKEN="$(cat /etc/rsc/workspace.json | jq --raw-output .co_token)"
OWNER_ID="$(cat /etc/rsc/workspace.json | jq --raw-output .owner_id)"
MESSAGE_API="$(cat /etc/rsc/workspace.json | jq --raw-output .co_user_api_endpoint)${OWNER_ID}/send_message/"
export WORKSPACE_NAME="$(cat /etc/rsc/workspace.json | jq --raw-output .workspace_name)"
export WORKSPACE_ID="$(cat /etc/rsc/workspace.json | jq --raw-output .workspace_id)"
MESSAGE="message=$(envsubst </etc/rsc/disk_full_template)"
curl --request POST --header "Authorization: ${CO_TOKEN}" --data "category=workspace_monitoring" --data "subject=Workspace file system almost full" --data "${MESSAGE}" "${MESSAGE_API}"
touch "${MESSAGE_SENT_FLAG_FILE}"
echo "Root file system almost full. Current usage: ${USAGE_PERCENTAGE}%"
