#!/bin/bash
set -e
MOUNT_NAME="$1"
USER="$2"
CO_USER_INFO_FILE="$3"

HOME_DIR="/home/${USER}"
MOUNT_POINT="${HOME_DIR}/${MOUNT_NAME}"

CO_USER_INFO=$(cat "${CO_USER_INFO_FILE}")

get_credentials () {
    if [ "${MOUNT_NAME}" == "researchdrive" ]; then
        RESEARCH_DRIVE_SECRET=$(echo "${CO_USER_INFO}" | jq --arg USERNAME "${USER}" -r '.[] | select(.username == $USERNAME) | .research_drive_secret')
        RESEARCH_DRIVE_CONFIG=$(echo "${CO_USER_INFO}" | jq --arg USERNAME "${USER}" -r '.[] | select(.username == $USERNAME) | .services | map(select(.service_meta.category=="research_drive")) | first')

        if [ "$RESEARCH_DRIVE_SECRET" == "null" ] && [ "$RESEARCH_DRIVE_CONFIG" == "null" ]
        then
            exit 1
        fi
        if [ "$RESEARCH_DRIVE_SECRET" != "null" ]; then
            jq -r -e '"\(.username//"") \(.token//"") \(.url//"")"' <<< "${RESEARCH_DRIVE_SECRET}"
        else
            jq -r -e '"\(.research_drive.username//"") \(.research_drive.webdav_token//"") \(.research_drive.url//"")"' <<< "${RESEARCH_DRIVE_CONFIG}"
        fi
    elif [ "${MOUNT_NAME}" == "webdav" ]; then
        WEBDAV_CONFIG=$(echo "${CO_USER_INFO}" | jq --arg USERNAME "${USER}" -r '.[] | select(.username == $USERNAME) | .services | map(select(.service_meta.category=="webdav")) | first')
        if [ "$WEBDAV_CONFIG" == "null" ]; then
            exit 1
        else
            jq -r -e '"\(.webdav.username//"") \(.webdav.webdav_token//"") \(.webdav.url//"")"' <<< "${WEBDAV_CONFIG}"
        fi
    fi
}

webdav_mount () {
    USERNAME="$1"
    PASSWORD="$2"
    URL="$3"

    CONFIG_FILE="${HOME_DIR}/.config/rclone/rclone-${USER}-${MOUNT_NAME}.conf"
    REMOTE_NAME="${USER}-${MOUNT_NAME}"
 
    if [ -z "${USERNAME}" ] && [ -z "${PASSWORD}" ]; then
        umount "$MOUNT_POINT"
        rm -rf "$MOUNT_POINT"
        rm -f "$CONFIG_FILE"
        exit 0
    fi

    sudo -u $USER mkdir -p "$MOUNT_POINT"
    mkdir -p "$(dirname "$CONFIG_FILE")"

    # Create the rclone configuration file
    cat > "$CONFIG_FILE" << EOL
[$REMOTE_NAME]
type = webdav
url = $URL
vendor = $MOUNT_NAME
user = $USERNAME
pass = $(rclone obscure $PASSWORD)
EOL
    chown -R ${USER}:${USER} ${HOME_DIR}/.config
    echo "rclone configuration for $REMOTE_NAME created successfully."

    sudo -u $USER mkdir -p "$MOUNT_POINT"
    sudo -u $USER RCLONE_CONFIG="${CONFIG_FILE}" rclone mount --daemon "$REMOTE_NAME:/" "$MOUNT_POINT"  --vfs-cache-mode writes
    echo "Successfully mounted $REMOTE_NAME at $MOUNT_POINT"
    if grep -qF "${REMOTE_NAME}:/ ${MOUNT_POINT}" /etc/fstab; then
        echo "Entry $MOUNT_POINT already exists in /etc/fstab"
    else
        USER_ID=$(id -u $USER)
        echo "${REMOTE_NAME}:/ ${MOUNT_POINT} rclone rw,defaults,uid=${USER_ID},gid=${USER_ID},umask=002,allow_other,nofail,_netdev,args2env,vfs_cache_mode=writes,config=${CONFIG_FILE},cache_dir=/var/cache/rclone 0 0" >> /etc/fstab
    fi
}

# Script start
case "${USER}" in
    root|ubuntu|"" )
        exit 0
    ;;
esac
echo "$(get_credentials)" | awk '{print $1" "$3}'
read -r username password url<<< "$(get_credentials)"
webdav_mount "${username}" "${password}" "${url}"
